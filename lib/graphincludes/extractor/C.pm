# This file is part of the DEPS/graph-includes package
#
# (c) 2005,2006 Yann Dirson <ydirson@altern.org>
# Distributed under version 2 of the GNU GPL.

package graphincludes::extractor::C;
use strict;
use warnings;

use base qw(graphincludes::extractor);

use graphincludes::params;
use File::Basename qw(dirname);

sub get_default_sysincludes {
  return ('/usr/include');
}

sub pattern { '\.([ch](pp|xx|\+\+)?|C|H|cc|hh)$' }

sub getdeps {
  my $self = shift;
  my ($graph) = @_;

  @ARGV = map {$_->{LABEL}} $graph->get_nodes();
  while (<>) {
    my $dstfile;
    if (m/^\s*#\s*include\s*"(.*)"/) {
      $dstfile = $self->locatefile ($1, dirname($ARGV), @graphincludes::params::inclpath);
    } elsif (m/^\s*#\s*include\s*<(.*)>/) {
      $dstfile = $self->locatefile ($1, @graphincludes::params::inclpath);
    } else {
      next;
    }

    if (defined $dstfile) {
      $graph->record_edge ($ARGV, $dstfile);
    } else {
      $self->record_missed_dep ($ARGV, $1);
    }
  }
}

1;
