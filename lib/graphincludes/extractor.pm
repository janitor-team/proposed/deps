# This file is part of the graph-includes package
#
# (c) 2005 Yann Dirson <ydirson@altern.org>
# Distributed under version 2 of the GNU GPL.

package graphincludes::extractor;
use strict;
use warnings;

use graphincludes::params;

sub pattern { '^$' }

sub accepts_file {
  my $class = shift;

  my $pat = $graphincludes::params::filename_regexp || ${class}->pattern;
  m/$pat/;
}

sub get_default_sysincludes {
  return ();
}

1;
