# This file is part of the DEPS/graph-includes package
#
# (c) 2006 Yann Dirson <ydirson@altern.org>
# Distributed under version 2 of the GNU GPL.

package DEPS::Style::Edge::WeightLabel;

use warnings;
use strict;

use base qw(DEPS::Style);
use Hash::Util qw(lock_keys unlock_keys);
use Carp qw(croak);

sub new {
  my $class = shift;
  my $self = $class->SUPER::new([], [],
				@_);

  unlock_keys (%$self);
  bless ($self, $class);
  lock_keys (%$self);
  return $self;
}

# FIXME: should be able to specify the level relative to which counting is done

sub apply {
  my $self = shift;
  my ($edge, $graphnode, $style) = @_;

  my $nedges = $edge->weight;
  # FIXME: be more friendly
  print STDERR "Warning; overriding label from DEPS::Style::Edge::WeightLabel"
    if defined $style->{label};
  $style->{label} = "[$nedges]";

  return $style;
}

1;
