Version 0.13 - 2006-01-08

 End-user-visible changes

 * Added a node style to display the number of ingredient nodes and of
   internal edges in a group (GroupStats), activated by default.

 * Updated documentation.

 Bugs fixed:

 * Ship all files (the 0.12 tarball missed all transforms and styles !)

 * The path-handling functions in the default project class were
   confused by leading "./" in paths and failed to idendify some
   dependencies.

 * The root graph can now be included in arguments to
   Transform::Consolidate, and "--consolidate 0-<N>" is now valid.

 * The tulip renderer had been broken by an API change in 0.12.

 * Workaround for a strange behaviour or ActivePerl 5.8.7 on Windows,
   which caused dependencies in subdirs not to be found.

 * Ingredientable::ingredients() method now always returns a list,
   even in scalar context when there is only one ingredient.

 Changes for the plugin writer:

 * New node style attributes "label" and "extralabel" for the dot
   renderer.

 * Added support for edge styling, and support for "label" attribute
   in the dot renderer.  New WeightLabel edge styler to replace old
   ad-hoc code.

 * Class moves and renames: transforms, styles.

==================================================

Version 0.12 - 2006-02-03

 End-user-visible changes

 * Start of a renaming of the whole toolkit to DEPS (Dependency
   Extraction and Processing System), graph-includes being only the
   current user frontend.

 * Graphs of grouped nodes now only include the defined groups.  The
   new --consolidate flag is now available to merge such graphs in a
   manner such that nodes of a given group-level, not included in any
   higher-level group, are shown as well.  As a side-effect, this flag
   also controls the highest group-level of the nodes to be drawn, in
   place of the --group flag.

 * The --group flag to render groups as node clusters is not available
   any more.  It will come back in a later release, under a more suitable
   name.

 * The documentation previously found in the README file is now
   available in DocBook XML format, in the doc/ directory.

 Bugs fixed:

 * Fixed the uniqueincludes project class, which has been broken since
   the 0.6 reorganization.

 * Calculation of edge weights was wrong.

 Changes for the class writer:

 * A project is now made of a set of graphs, themselves connected as a
   graph representing the transformations used to produce them from the
   other graphs.  Most APIs changed consequently.

 * Projects are now expected to provide a nlevels() method matching
   the number of grouping levels defined, and the filelabel() methods
   are now allowed to return undef when a node does not belong to any
   group at the requested level.

 * Arbitrary transformations and styles can now be defined, although
   no facility is provided yet by the master graph-includes script to
   apply them, short of editing the script.  Currently available
   transformations are "compatgroup" (use the old filelabel() mechanism
   to define grouping levels), "consolidate" (merge several group levels
   in a single graph to show non-grouped nodes as well), and
   "transitivereduction".  The only currently available styler allows to
   apply style attributes (like colors) to a node according to the group
   it belongs to at a given grouping level.

 Under the hood:

 * Objects now get their hash locked in the constructors, to catch
   typos, unwanted modifications, and other errors likely to occur
   during subsequent refactorings.

 * Introduced graph, node, and edge objects (splitted out from
   project, along with relevant methods).  A project now has a stack
   of graphs.

 * Many other changes, fixing some bugs, and introducing brand new ones.

==================================================

Version 0.11 - 2005-12-06

 End-user-visible changes

 * Generalized support for node coloring, with renderer::dot
   supporting 2 coloring levels (background and outline)

 * Improved portability to non-UNIX platforms, was successfully run on
   win32.

 * The C extractor now looks in for system headers in /usr/include by
   default.

 * The Perl extractor uses the default @INC value to locate system
   headers.

 Changes for the class writer:

 * Per-language extractors can now declare a default system-include
   path.

 Bugs fixed:

 * The --color flag was broken.

==================================================

Version 0.10 - 2005-11-29

 End-user-visible changes

 * Added a --renderer flag to select another renderer than dot.

 * Added preliminary support to use tulip as renderer.

==================================================

Version 0.9.2 - 2005-11-2

 * Start of a "tutorial" or "how to use" section in the doc.

 * Explicitely written down the plan for the upcoming redesign.

==================================================

Version 0.9.1 - 2005-06-01

 Bugs fixed:

 * Fixed a typo preventing the recursive search to see .c and .h
   files.

==================================================

Version 0.9 - 2005-06-01

 Bugs fixed:

 * Now fails nicely when no dependencies are found for any reason.

 Other end-user-visible changes

 * Command-line and version are now logged in the report file.

 * New --version flag.

 * Can now be run from source dir with no effort.

 * Exit with usage on command-line.

 * Updated usage summary with recent options.

==================================================

Version 0.8 - 2005-05-26

 End-user-visible changes

 * Directory arguments are now searched for files, allowing to handle
   projects with a large number of files.

 * New --fileregexp option to override the per-language regexp used to
   look for files inside directories.

 * More statistics are available in the report file.

 Changes for the class writer:

 * extractors' getdeps methods cannot rely any more on @ARGV
   containing the files list, and must get it from the FILES instance
   variable.

==================================================

Version 0.7 - 2005-05-10

 Bugs fixed:

 * The '.graph-includes.report' suffix used when using --output did
   not include the first dot.

 Other end-user-visible changes

 * Visible edges are now labelled with the number of real dependencies
   they represent, so we have some visual feedback of transitive
   reduction.

 * Huge performance boost for the transitive reduction.

 * When cycles are present, the specific reduction selected is usually
   a different one than in 0.6.1 and below, as a side-effect of
   implementation changes.

 * More documentation has been written.

 Changes for the class writer:

 * project::record_missing_dep was split out of project::record_dep.

 * Dependencies are now stored in a hash instead of an array.

 * The "label" special_edge attribute is now an array of strings, to
   be presented on several lines.

==================================================

Version 0.6.1 - 2005-04-26

 Bugs fixed:

 * Fixed the distribution which misses the C and perl extractors.

==================================================

Version 0.6 - 2005-04-24

 Bugs fixed:

 * A typo in the graphincludes::params package prevented the
   parameters default values to be used.

 Command-line changes:

 * New --language flag to select syntax of source files.

 * Preliminary support for extracting perl dependencies, using
   "--language perl"

 Changes for the class writer:

 * C-specific behaviour is now located in new extractor::C class.

 * Changed special_edge() return value to an hash of graphviz node
   attribute/value pairs

 * More methods were moved from project::default up to ancestor
   classes.

==================================================

Version 0.5 - 2005-04-20

 Command-line changes:

 * --Include is now recognized as long form for -I.

 * New --sysInclude option to specify system directories.  Included
   files not found in the project, but found in those directories are
   not considered as "not found".

 Other end-user-visible changes

 * Removed inconditional duplicate warnings flooding the output.

 * Verbose diagnostics are now output unconditionally into a
   graph-includes.report (or <file>.graph-includes.report) file.

 Changes for the class writer:

 * The constructor for the project classes now uses named parameters,
   instead of positional parameters.
 * Global parameters were moved to a new graphincludes::params package.

==================================================

Version 0.4 - 2005-04-17

 Command-line changes:

 * Implemented cpp-like "-I <dir>" syntax for #include lookup,
   dropping the infamous former ad-hoc heuristic.

 Other end-user-visible changes

 * `#include "..."' are now analyzed using the standard cpp semantics,
   and `#include <...>' line are now considered as well.

 Under the hood

 * Many code cleanups.

==================================================

Version 0.3 - 2005-04-14

 Bugs fixed:

 * Fixed a bug in the ad-hoc #include resolver which caused some
   dependencies to be ignored

 Command-line changes:

 * New --paper option to get a graph printable on paper (for now, only
   a4, a3 and letter)

 Other end-user-visible changes

 * More documentation has been written.

==================================================

Version 0.2 - 2005-04-01

 Bugs fixed:

 * --output would reject its argument.

 * --focus and --showdropped caused invalid output.

 Command-line changes:

 * New --prefixstrip option to make the graph more readable by
   stripping a common prefix from all filenames.

 * Obsolete (pre 0.1 !) --allfiles option was completely dropped.

 Other end-user-visible changes

 * More documentation has been written.

 * The default project-class (and the sample wesnoth class) now
   provide default singleton level-2 groups (so "--group 2-2" should
   work on all projects).

 Changes for the class writer:

 * The tool expect a project class to build its dependency graph in an
   init() method, instead of in the constructor.

 * The project class constructor now takes an additional prefixstrip
   argument.  More changes to come in this area to cause such changes
   to be less disruptive in the future.

 * A project class can now specify excuses for abusive dependencies,
   to be shown in as edges of a different color, and with the excuse
   as a label.

==================================================

Version 0.1 - 2005-03-28

 Initial public release.
